"""
This short script to preview samples, produced by the MCMC.py
see visualization.ipynb for the full visualization
"""

import data_pers
from MCMC import TRIAL_N

print( f'name of the file: {TRIAL_N}')

# Load warmup iterations
try:
    H = data_pers.Load(TRIAL_N, files=[0])
    print(  '-- warm-up file found --')
    print( f'it have {len(H)} iterations' )
    print( f'Posterior range: from {H["Posterior"].min():.2f} to {H["Posterior"].max():.2f}' )
    print( 'summary' )
    print( H.info() )
except:
    print(  'warm-up file not found')

print()

# loading main iterations
try:
    H = data_pers.Load(TRIAL_N, files=[1])
    print(  '-- main file found --')
    print( f'it have {len(H)} iterations' )
    print( f'Posterior range: from {H["Posterior"].min():.2f} to {H["Posterior"].max():.2f}' )
    print( 'summary' )
    print( H.info() )
except:
    print(  'main file not found')
