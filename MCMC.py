from datetime import datetime

from numpy import exp, isnan, isinf

import ram
import data_pers
import model
from useful import rexp, format_timedelta
from read_data import MODEL_T


TRIAL_N = 'C:/MCMC_output/covid_model'
# directory to save MCMC output


MCMC_UPDATE_BATCHES = [
    ('Ro_w', 'log_start',),
    ('log_p_a', ),
    #('logit_det_rate_a', ),  # in the main variant of the model, confirmed cases are not used
    ('log_hos_rate_a', ),
    ]


def MAIN(trial_name=TRIAL_N,
         warmup=5_000,
         iterations_n=200_000,
         thinning=10):
    """
    Markov chain Monte Carlo.

    Chain would contain `warmup` + `iterations_n`.
    Output would be saved to `trial_name` folder as two files, corresponding to warmup and main iterations.
    Only every `thinning` iteration is recorded.
    """
    # prepare files to record MCMC output
    H = data_pers.ParallelDataWriter(trial_name, key='n')
    print()
    print(trial_name)
    print()

    # set up
    epidemic      = model.Epidemic()
    epidemic_test = model.Epidemic()
    epidemic_pred = model.Epidemic(T=MODEL_T+7)
    print('started with Likelihood', epidemic.Likelihood)
    print('and Prior', epidemic.Prior)
    if isnan(epidemic.Likelihood) or isinf(epidemic.Likelihood):
        raise Exception('Starting with zero Likelihood')
    if isnan(epidemic.Prior) or isinf(epidemic.Prior):
        raise Exception('Starting with zero Prior')

    # initialize proposal variance
    jump = {batch: ram.RobustAdaptiveMetropolis(len(epidemic.get_params(batch))) for batch in MCMC_UPDATE_BATCHES}

    # initialize timer
    time_start = datetime.now()
    milestones =  [x for i in range(1, 7) for x in [1*10**i, 2*10**i, 5*10**i]]

    print(f'ready at {time_start:%Y-%m-%d %H:%M}')

    for iteration in range(iterations_n + warmup):

        ####################
        ##   MCMC sample  ##
        ####################
        for batch in MCMC_UPDATE_BATCHES:
            epidemic_test.copy(epidemic)

            old_vals = epidemic_test.get_params(batch)
            new_vals = old_vals + jump[batch].draw()
            epidemic_test.set_params(batch, new_vals)

            jump_prob = epidemic_test.Posterior - epidemic.Posterior
            if isnan(jump_prob):
                jump_prob = float('-inf')

            jump[batch].adapt(min(1, exp(jump_prob)), iteration)

            if jump_prob > -rexp(1):
                # jump is succesfull! replase current values
                epidemic_test, epidemic = epidemic, epidemic_test

        ####################
        ## SAVE ITERATION ##
        ####################
        if iteration == warmup:
            # when warmup is over, start writing into a new file
            H.new_file()
            print('started main iterations with Likelihood', epidemic.Likelihood)
            print('and Prior', epidemic.Prior)

        if iteration % thinning == 0:
            # save MCMC sample and some additional quantities
            epidemic_pred.copy(epidemic)
            epidemic_pred.clear()
            H.writerow(
                Prior=epidemic.Prior,
                Likelihood=epidemic.Likelihood,
                Posterior=epidemic.Posterior,

                **epidemic.parameters,
                **epidemic.for_record(),
                **epidemic_pred.predictions()
                )

        ####################
        ## print progress ##
        ####################
        if iteration+1 in milestones:
            time_now = datetime.now()
            time_per_inter = (time_now - time_start) / (iteration + 1)
            time_end = time_now + time_per_inter*(iterations_n + warmup - iteration - 1)
            msg = f'{time_now:%H:%M} -- iter {iteration+1} -- expected in {time_end:%H:%M} (+{format_timedelta(time_end-time_now)}) -- {format_timedelta(time_per_inter)}/i'
            print(msg)

    print('done!')
    time_now = datetime.now()
    time_per_inter = (time_now - time_start) / (iterations_n + warmup)
    print(f'{time_now:%H:%M} -- total time: {format_timedelta(time_now-time_start)} -- {format_timedelta(time_per_inter)}/i')


if __name__ == '__main__':
    MAIN()
