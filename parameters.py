import datetime

from numpy import exp, log
from numpy import array

from useful import compute_unit_NGM, largest_l_eigenvector, logit, logistic
from prob_prog import Model, derived_quantity, prior, basic_transformation

# Number of age groups, days and weeks in the model
from read_data import MODEL_n, MODEL_T, MODEL_W
from read_data import DATA_DATES
# contact matrises
from read_contacts import C_PER_DAY_July, C_PER_DAY_Sept, C_PER_DAY_Dec


# Prior for hospitalization rates per age group, French estimates
# https://science.sciencemag.org/content/369/6500/208
# Table S 1
# we devide these estimates by 3 to represent the lower severity of the omicron variant
PRIOR_HOS_RATE = [x/100/3 for x in [0.1, 0.1, 0.5, 1.1, 1.4, 2.9, 5.8, 9.3, 26.2]]

# Prior for detection rates per age group, assume 5%
PRIOR_DET_RATE = [1/20   for x in range(MODEL_n)]


class Parameters(Model):

    T: 'constant' = MODEL_T  # number of days in the model
    scenario: 'constant' = 'avg'  # scenario to use for prognosis

    #########
    ## NGM ##
    #########

    Ro_w: 'free parameter' = [1.7 for t in range(MODEL_W)]  # Ro per week

    @prior
    def prior_Ro(Ro_w):
        if any(x<0 for x in Ro_w):
            return float('-inf')
        change = [a-b for a, b in zip(Ro_w, Ro_w[1:])]
        return sum( -x**2 * 50 for x in change)

    @basic_transformation
    def last_average_Ro(Ro_w):
        return sum(Ro_w[-9:]) / 9

    @derived_quantity
    def Ro_t(Ro_w, last_average_Ro, T, scenario):
        """ Ro per day. Derived by exptrapolation Ro_w -- Ro per week"""
        Ro_t = [(a*(7-i) + b*i) / 7 for a, b in zip(Ro_w, Ro_w[1:]) for i in range(7)]
        Ro_t = [Ro_w[0]]*7 + Ro_t + [Ro_w[-1]]*7
        Ro_t = Ro_t[:MODEL_T]

        # in case we are running model for predictions, the simulated days is longer that available data
        # T > MODEL_T
        # We fill the Ro_t for these days, based on a scenario
        if scenario == 'avg':
            Ro_t += [last_average_Ro]*(T-MODEL_T)
        elif scenario == 'as now':
            Ro_t += [Ro_t[-1]]*1000
        elif scenario == 'avg + new strain':
            increment = [0]*MODEL_T +  [logistic(i/7) for i in range(-21, 31)] + [1]*1000
            increment = array(increment)*0.5 + 1
            Ro_t += [last_average_Ro]*1000
            Ro_t = list(Ro_t[:T]*increment[:T])
        elif scenario == 'as now + new strain':
            increment = [0]*MODEL_T +  [logistic(i/7) for i in range(-21, 31)] + [1]*1000
            increment = array(increment)*0.5 + 1
            Ro_t += [Ro_t[-1]]*1000
            Ro_t = list(Ro_t[:T]*increment[:T])
        return Ro_t[:T]

    # relative susceptibilities by age
    log_p_a: 'free parameter' = [0] * MODEL_n
    # prior is log-normal with std=1/2, with age groups 0-19 years a-priory twise less susceptible
    # as the mean of the lognormal is exp(m + std**2/2) = exp(m + 1/8), we use a correction of 1/8
    prior_log_p_a: 'prior' = lambda Y: sum(-(y - m + 1/8)**2 * 2 for y, m in zip(Y, [-0.693, -0.693, 0, 0, 0, 0, 0, 0, 0]))

    @derived_quantity(outputs=('transmission_matr', 'first_eigenvector'))
    def contact_matrs(p_a):
        """
        Normalized tramsmission matrices (contact matrices times age-specific correction)
        and leading eigenvector of the first contact matrix
        """
        # for this model, we use only one contact matrix, from December 2020
        Dec_matr = [[C_PER_DAY_Dec[i, s] * p_a[s] for s in range(MODEL_n)] for i in range(MODEL_n)]
        Dec_unit_Ro, eigenvector = largest_l_eigenvector(Dec_matr)
        Dec_matr = array(Dec_matr) / Dec_unit_Ro
        return Dec_matr, eigenvector

    @derived_quantity
    def NGM_t(Ro_t, transmission_matr, T):
        """ Next generation matrixes per day"""
        # does the same as  [transmission_matr * Ro for Ro in Ro_t]
        return transmission_matr * array(Ro_t)[:, None, None]

    #########################
    ##  vaccines efficacy  ##
    #########################

    @derived_quantity
    def sus_multiplier_vaccine():
        """ susceptibility multiplicators due to vaccine effect """
        # for technical reasons, all values here are enclosed in [ ]
        # this represents that all age groups have the same reduction factor
        omicron = array([
            [1] * MODEL_n,     # no vaccine
            [0.8] * MODEL_n,   # got first dose
            [0.7] * MODEL_n,   # at least 1 week passed since second dose
            [0.6] * MODEL_n,   # at least 1 week passed since third dose
            ])
        return omicron

    @derived_quantity
    def severity_multiplier_vaccine():
        """ severity multiplicators due to vaccine effect """
        # for technical reasons, all values here are enclosed in [ ]
        # this represents that all age groups have the same reduction factor
        return array([
            [1],     # no vaccine
            [0.5],  # got first dose
            [0.3],   # at least 1 week passed since second dose
            [0.2],   # at least 1 week passed since third dose
            ])

    #########################
    ##   initial values    ##
    #########################

    log_start: 'free parameter' = 1

    @derived_quantity
    def in_con_a(start, first_eigenvector):
        """
        This parameter is used to define the initial condition of the model:
        number of people in conpartments I and E at time t=0
        """
        return start * first_eigenvector


    ###################################
    ## detection and hospitalization ##
    ###################################

    logit_det_rate_a: 'free parameter' = [logit(x) for x in PRIOR_DET_RATE]
    # logit normal prior, std = 1/2
    prior_logit_det_rate_a: 'prior' = lambda Y: -sum((       y        )**2 * 2 for y in Y)

    log_hos_rate_a:   'free parameter' = [log(x) for x in PRIOR_HOS_RATE]
    # log normal prior, std = 1/2
    # as the mean of the lognormal is exp(m + std**2/2) = exp(m + 1/8), we use a correction of 1/8
    prior_log_hos_rate_a:   'prior' = lambda Y: -sum((y - log(m) + 1/8)**2 * 2  for y, m in zip(Y, PRIOR_HOS_RATE))


    ###########################
    ## extra transformations ##
    ###########################
    # these derived quantities not used for simulation, but usefull for visualization

    @basic_transformation
    def sus_a(p_a):
        return (p_a / p_a[-1])[:-1]

    @basic_transformation
    def hos_det_rate_a(hos_rate_a, det_rate_a):
        return array(hos_rate_a) / det_rate_a


if __name__ == '__main__':
    par = Parameters()
    print( repr(par))
    print( par.Prior )
    print( par.all_params_with_prior() )
    print( par.transformed_pars() )
    par.list_dependencies()
    print( par.all_priors() )
