import numpy
from read_data import POP_SIZES

AGE_GROUPS = [f'{i}-{i+9}' for i in range(0, 80, 10)] + ['80+']


# initial matrixes:
# column - target age group
# row - sourse age group

C_PER_DAY_July = [
    [1.59, 0.22, 0.25, 0.95, 0.37, 0.32, 0.22, 0.17],
    [0.23, 1.39, 0.29, 0.19, 0.65, 0.34, 0.16, 0.15],
    [0.29, 0.32, 1.69, 0.54, 0.39, 0.5, 0.28, 0.17],
    [1.2, 0.22, 0.57, 1.38, 0.48, 0.53, 0.39, 0.22],
    [0.44, 0.7, 0.38, 0.45, 0.99, 0.58, 0.32, 0.35],
    [0.41, 0.41, 0.54, 0.54, 0.64, 0.9, 0.48, 0.3],
    [0.29, 0.19, 0.3, 0.48, 0.34, 0.47, 0.79, 0.48],
    [0.27, 0.21, 0.22, 0.28, 0.46, 0.37, 0.58, 0.8]]

C_PER_DAY_Sept = [
    [3.24, 0.39, 0.21, 1.1, 0.58, 0.09, 0.2, 0.12],
    [0.42, 3.78, 0.16, 0.33, 0.71, 0.34, 0.09, 0.05],
    [0.26, 0.18, 1.71, 0.52, 0.32, 0.48, 0.17, 0.11],
    [1.39, 0.39, 0.55, 1.2, 0.74, 0.57, 0.39, 0.22],
    [0.68, 0.78, 0.32, 0.69, 1.05, 0.7, 0.31, 0.28],
    [0.12, 0.41, 0.52, 0.58, 0.76, 1.12, 0.48, 0.24],
    [0.25, 0.1, 0.19, 0.39, 0.33, 0.48, 0.84, 0.41],
    [0.19, 0.07, 0.15, 0.27, 0.37, 0.28, 0.5, 0.94]]

C_PER_DAY_Dec = [
    [2.44, 0.43, 0.20, 0.68, 0.56, 0.23, 0.19, 0.08],
    [0.46, 3.02, 0.21, 0.36, 0.68, 0.33, 0.19, 0.09],
    [0.24, 0.23, 1.35, 0.34, 0.37, 0.47, 0.20, 0.10],
    [0.86, 0.43, 0.36, 1.02, 0.61, 0.59, 0.35, 0.20],
    [0.66, 0.74, 0.36, 0.57, 1.02, 0.62, 0.35, 0.20],
    [0.29, 0.39, 0.51, 0.60, 0.68, 0.83, 0.39, 0.19],
    [0.24, 0.22, 0.21, 0.36, 0.38, 0.39, 0.63, 0.29],
    [0.13, 0.13, 0.13, 0.25, 0.27, 0.23, 0.36, 0.46]]


for C in C_PER_DAY_July, C_PER_DAY_Sept, C_PER_DAY_Dec:
    # transpose
    C[:] = [[C[i][j] for i in range(8)] for j in range(8)]

    # original matrixes are symmetrized for the whole Finland
    # simmetrize them for the given region
    corrections = numpy.array([576_696, 601_705, 675_248, 706_147, 657_943, 732_782, 724_769, 539_919, 302_710]) / POP_SIZES
    corrections = corrections / numpy.mean(corrections)
    C[:] = [[C[i][j]*corrections[i] for j in range(8)] for i in range(8)]

    # add one more age group: split 70+ into 70-79 and 80+
    ratio = POP_SIZES[7] / (POP_SIZES[7] + POP_SIZES[8])
    for a in range(8):
        C[a][-1:] = [C[a][-1]*ratio, C[a][-1]*(1-ratio)]
    C += [C[-1]]

# final matrixes:
# row - sourse age group
# column - target age group

C_PER_DAY_July = numpy.array(C_PER_DAY_July)
C_PER_DAY_Sept = numpy.array(C_PER_DAY_Sept)
C_PER_DAY_Dec = numpy.array(C_PER_DAY_Dec)


if __name__ == '__main__':
    print(C_PER_DAY_July)
    print(POP_SIZES)
