This files contain one of the models, used by THL COVID modelling group.

This model used for analysing a wave of COVID-19 outbreak in Finland during January -- February 2022.

Most recent model can be found at:
https://gitlab.com/2pi360/covid_model_finland_ii_wave_public/-/tree/master

Link for this model:
https://gitlab.com/2pi360/covid_model_finland_ii_wave_public/-/tree/version/2022_01

Previous model used for the second wave can be found at
https://gitlab.com/2pi360/covid_model_finland_ii_wave_public/-/tree/version/2021_02
https://gitlab.com/2pi360/covid_model_finland_ii_wave_public/-/tree/version/2021_03
https://gitlab.com/2pi360/covid_model_finland_ii_wave_public/-/tree/version/2021_04
https://gitlab.com/2pi360/covid_model_finland_ii_wave_public/-/tree/version/2021_05
https://gitlab.com/2pi360/covid_model_finland_ii_wave_public/-/tree/version/2021_09
https://gitlab.com/2pi360/covid_model_finland_ii_wave_public/-/tree/version/2021_10

Model used during the first wave can be found at
https://gitlab.com/2pi360/covid_model_finland_public


# LIST OF FILES:

COVID_MCMC_II_season.pdf
- model description

MCMC.py
- main file. Running it will start the inference, which creates the output file with posterior samples

parameters.py
- describes the parameter model

model.py
- describes the transmission and observation model

prob_prog.py
- provides Model class, which allows to define models with a declarative language.

read_data.py
- contains pseudodata

read_contacts.py
- contains contact matrices

extra_data.py
- contains different empirical distribution used for modelling, such as distribution of time to detection, time to hospitalization, time spend in hospital etc.

useful.py
- collection of different utility functions

data_pers.py
- utility functions for reading and writing MCMC samples

visualization.ipynb
- small demo of now the visualization is made

fact_cov19cov.csv
- dataset describing the numbers of vaccinated individuals, as download from public source


# DATA AND VISUALIZATION

Due to limitations, parts of the code responsible for loading the data and visualizing the results were not provided.
The actual data used for inference is replaced by pseudodata. Only a demo of the visualization code is provided.


# INSTRUCTIONS

Run MCMC.py

The MCMC samples would be saved into 'C:/history/covid_modelling' folder, or any other folder defined by TRIAL_N in MCMC.py line 11.
The inference can take a hour, but the first (warm-up) samples should be available immediately
and could be read/analysed.

To read, analyse and visualize the produces samples, see visualization.ipynb (jupyted notebook file).
Alternatevely, one can run preview_samples.py to see that samples exists.
Samples are loaded as a pandas.DataFrame object, to save them into a different format see pandas.DataFrame documantaion.

Sometimes a warning appears: "RuntimeWarning". It is produces by numpy, as MCMC tries to samples from invadid regions of the parameter space.
This should not cause any problems.


# REQUIREMENTS

Code was tested on

```
Python 3.6.1

numpy==1.12.1
scipy==0.19.0
pandas==0.20.1
numba==0.33.0
```
