from math import log, exp, inf

import numpy
import numba
from numpy import array, zeros, convolve

import useful
from parameters import Parameters
from prob_prog import derived_quantity, likelihood_factor

# Read data from the data sourses
# Number of age groups and days
from read_data import MODEL_n, MODEL_T, MODEL_V
# Datasets: numbers of confirmed, hospitalized, ICU and fatal cases distributed by age group and day
from read_data import CONF, WARD, ICUC, DEAD
# Datsets: number of vaccine distributed by dose, age group and day.
# `_PAST` data referes to vaccines distributed before the modelled period
from read_data import VACS, VACS_PAST
# Datsets: population size of
from read_data import VAC_POP, VAC_POP_PAST
# Population size
from read_data import POP_SIZES, POP_SIZE

# Distribution of time to outcome
from extra_data import CONF_DISTR, HOSP_DISTR, ICU_DISTR, DEAD_DISTR
# Distribution of time spend in ICU and hospital ward
from extra_data import WARD_DURATION_DISTR, ICU_DURATION_DISTR


def convolve_infections(new_E_vat, past_E, delay_a, T):
    """
    Convolution, used in observation model.
    """
    expected = zeros((MODEL_V, MODEL_n, T))
    for v in range(MODEL_V):
        for a in range(MODEL_n):
            infections = numpy.concatenate([past_E[v, a, :], new_E_vat[v, a, :]])
            expected[v, a, :] = convolve(infections, delay_a[a])[30:T+30]

    return expected


class Epidemic(Parameters):

    ###########################
    ##     vaccine data      ##
    ###########################

    @derived_quantity
    def vaccs_dat(T, scenario):
        # Number of people, recieving the vaccine.
        # This is mostly just a copy of a corresponing dataset.
        # Hovewer, when making forecasts, scenario is applies
        vaccs_dat = zeros((3, MODEL_n, T))
        vaccs_dat[:, :, :MODEL_T] = VACS
        vac_pop = VAC_POP[:, :, -1]

        for t in range(MODEL_T, T):
            # scenario!
            # vaccinate as in the last day in the data
            vacc = zeros((3, MODEL_n), int)

            for v in range(3):
                overload = 0
                for a in range(8, -1, -1):
                    max_doses = max(0, vac_pop[v, a]//10) # never vaccinate more than 10% of the remaining category
                    doses = VACS[v, a, -1] + overload
                    if doses > max_doses:
                        overload, doses = doses - max_doses, max_doses
                    else:
                        overload = 0
                    vacc[v, a] = doses

            vaccs_dat[:, :, t] = vacc
            vac_pop += [- vacc[0],
                        + vacc[0] - vacc[1],
                                  + vacc[1] - vacc[2],
                                            + vacc[2]]

        return vaccs_dat


    ###########################
    ##       SEIR model      ##
    ###########################

    @derived_quantity(outputs='S E I R new_E past_E'.split())
    def hidden_states(NGM_t, in_con_a, vaccs_dat, sus_multiplier_vaccine, T):
        """
        Simulate the hidden state of the epidemic (true numbers of infected) with a given set of parameters.

        inputs:
        NGM_t: contact matrix for each day
        in_con_a: initial conditions of the model per age group
        vaccs_dat: number of people, recieving the vaccine, per dose, age group and day
        sus_multiplier_vaccine: susceptibility multiplicator by vaccine status
        T: number of days in the simulation

        outputs:
        S, E, I, R - numbers of individuals in corresponing compartments by age group and day
        new_E - numbers of new exposed individuals (i.e. new infections) by vaccine, age group and day
        past_E - numbers of new exposed individuals, during 30 days before modelled period
        """
        # pre-set parameters
        E_dur = 3
        I_dur = 5

        # init arrays
        S_vat = zeros((MODEL_V, MODEL_n, T))        # susceptible, by vaccination status
        E_at = zeros((MODEL_n, T))                  # exposed (here it mean "infected, but not yet infectious")
        I_at = zeros((MODEL_n, T))                  # infectious
        R_at = zeros((MODEL_n, T))                  # removed
        new_E_vat = zeros((MODEL_V, MODEL_n, T))    # new exposed per day, by vaccination status
        past_E_vat = zeros((MODEL_V, MODEL_n, 30))  # new exposed per day, by vaccination status, for extra modelled period

        # intitial state
        R = in_con_a * 0
        E = in_con_a * E_dur
        I = in_con_a * I_dur

        # susceptible by vaccination status
        # we start 30 days before the modelled period
        S = VAC_POP_PAST[:, :, -30]

        # Simulations
        # Simulate for 30 extra days
        for t in range(-30, T):
            # move numbers between S, E, I and R compartments
            S_to_E = S * sus_multiplier_vaccine * (I @ NGM_t[max(t, 0)] / I_dur / POP_SIZES)
            E_to_I = E / E_dur
            I_to_R = I / I_dur

            S = S - S_to_E
            E = E + sum(S_to_E) - E_to_I
            I = I               + E_to_I - I_to_R
            R = R                        + I_to_R

            # vaccines
            first_dose  = vaccs_dat[0, :, t]   if t >= 0  else VACS_PAST[0, :, t]
            second_dose = vaccs_dat[1, :, t-7] if t >= 7  else VACS_PAST[1, :, t-7]
            third_dose  = vaccs_dat[2, :, t-7] if t >= 7  else VACS_PAST[2, :, t-7]
            S += [-first_dose,
                  +first_dose - second_dose,
                              + second_dose - third_dose,
                                            + third_dose]

            # fix potential problems
            S[S<0] = 0

            # save values
            if t >= 0:
                I_at[:, t] = I
                E_at[:, t] = E
                S_vat[:, :, t] = S
                R_at[:, t] = R
                new_E_vat[:, :, t] = S_to_E
            else:
                past_E_vat[:, :, t] = S_to_E

        return S_vat, E_at, I_at, R_at, new_E_vat, past_E_vat

    # convolutions of infections, used to compute expected cases
    @derived_quantity
    def conv_conf(new_E, past_E, T): return convolve_infections(new_E, past_E, CONF_DISTR, T)
    @derived_quantity
    def conv_hosp(new_E, past_E, T): return convolve_infections(new_E, past_E, HOSP_DISTR, T)
    @derived_quantity
    def conv_icuc(new_E, past_E, T): return convolve_infections(new_E, past_E, ICU_DISTR, T)
    @derived_quantity
    def conv_dead(new_E, past_E, T): return convolve_infections(new_E, past_E, DEAD_DISTR, T)

    @derived_quantity
    def new_conf(conv_conf, det_rate_a):
        """ Expected number of confirmed cases per age group and day """
        return conv_conf * det_rate_a[:, None]

    @derived_quantity
    def new_hosp(conv_hosp, hos_rate_a, severity_multiplier_vaccine):
        """ Expected number of hospitalizations per age group and day """
        return conv_hosp * hos_rate_a[:, None] * severity_multiplier_vaccine[:, :, None]

    ####################################
    ## Likelihood function components ##
    ####################################

    # in the main variant of the model, confirmed cases are not used
    #@likelihood_factor
    #def likelihood_detection(new_conf):
        ## two last days of confirmed cases are ignored, as this data tend to be late
        #return dl_negabinom_up_to_c(CONF[:, :, :-2], new_conf[:, :, :-2], 0.3).sum()

    @likelihood_factor
    def likelihood_hospitalization(new_hosp):
        return dl_negabinom_up_to_c(WARD[:, :, :-7], new_hosp[:, :, :-7], 0.1).sum()

    ####################################
    ## Other functions                ##
    ####################################

    def for_record(self):
        """
        Return some additional metrics, to be recorded on every MCMC iteration
        """
        return {
            'S_at_the_end': (self.S[:, :, -1] * self.sus_multiplier_vaccine).sum() / POP_SIZE,
            'E_at_the_end': sum(self.E[:, -1]) / POP_SIZE,
            'I_at_the_end': sum(self.I[:, -1]) / POP_SIZE,
            'R_at_the_end': sum(self.R[:, -1]) / POP_SIZE,

            'attack_rate': self.new_E.sum() / POP_SIZE,
            'current_Re': self.R_effective[-1],
            'Re': self.R_effective[::7]
            }

    def predictions(self):
        """ Predict the number of hospitalizations during the next week """
        return {
            'p_hosp_7': useful.rpolya(self.new_hosp[:, :, -7:].sum(), over=0.1),
            }

    # Compute extra quantities which are not used to compute likelihood, but could be used for visualization and analysis.

    @derived_quantity
    def S_weighted(S, sus_multiplier_vaccine):
        """ Susceptible by age and day, taking into accound different susceptibilities by vaccine group """
        return sum(S * sus_multiplier_vaccine[:, :, None])

    @derived_quantity
    def R_weighted(S, R, sus_multiplier_vaccine):
        """ Removed by age and day, taking into accound different susceptibilities by vaccine group """
        return R + sum(S * (1-sus_multiplier_vaccine)[:, :, None])

    @derived_quantity
    def new_icuc(conv_icuc):
        """ Expected number of ICU hospitalizations per age group and day """
        return conv_icuc * (ICUC[:, :, :MODEL_T].sum(2) / (conv_icuc[:, :, :MODEL_T].sum(2)+0.01))[:, :, None]

    @derived_quantity
    def new_dead(conv_dead):
        """ Expected number of fatal cases per age group and day """
        return conv_dead * (DEAD[:, :, :MODEL_T].sum(2) / (conv_dead[:, :, :MODEL_T].sum(2)+0.01))[:, :, None]

    # Compute burdens

    @derived_quantity
    def hosp_burden(new_hosp, T):
        """ Expected number of occupied hospital beds per day """
        hosp_burden = [0]*T
        for a in range(9):
            hosp_burden += convolve(new_hosp[:, a, :].sum(0), WARD_DURATION_DISTR[a])[:T]
        return hosp_burden

    @derived_quantity
    def icu_burden(new_icuc, T):
        """ Expected number of occupied ICU beds per day """
        icu_burden = [0]*T
        for a in range(9):
            icu_burden += convolve(new_icuc[:, a, :].sum(0), ICU_DURATION_DISTR[a])[:T]
        return icu_burden

    @derived_quantity
    def R_effective(T, S, NGM_t, sus_multiplier_vaccine):
        # compute effective Ro for all days
        R_effective = [0] * T
        for t in range(T):
            S_t = sum(S[:, :, t] * sus_multiplier_vaccine)  # number of susceptibles, taking into accound vaccination
            C = array([[NGM_t[t][i, s] * S_t[s] / POP_SIZES[s] for s in range(MODEL_n)] for i in range(MODEL_n)])
            R_effective[t] = useful.largest_eigenvalue(C)
        return R_effective

    def _synthetic_data(self):
        conf = [[[useful.rpois(self.new_conf[v, a, t]) for t in range(210)] for a in range(9)] for v in range(4)]
        ward = [[[useful.rpois(self.new_hosp[v, a, t]) for t in range(210)] for a in range(9)] for v in range(4)]
        icuc = [[[useful.rpois(self.new_icuc[v, a, t]) for t in range(210)] for a in range(9)] for v in range(4)]
        dead = [[[useful.rpois(self.new_dead[v, a, t]) for t in range(210)] for a in range(9)] for v in range(4)]
        return conf, ward, icuc, dead


# Probability distributions are optimized: constants (i.e. factors that do not depend on parameters) are not computed,
# border assumptions of paramters are not tested.
# Also, function and vectorized using numba, so they can be applied to arrays and matrixes

@numba.vectorize([numba.float64(numba.int32, numba.float64)])
def dl_poisson_up_to_c(x, p):
    """
    log PMF of a Poisson distribution, up to a constant. Assumes p != 0

    the expression with constants would be:
               x*log(p) - p - log(x!)
    """
    if x==0:
        return -p
    else:
        return x*log(p) - p


@numba.vectorize([numba.float64(numba.int32, numba.int32, numba.float64)])
def dl_binom_up_to_c(x, n, p):
    """
    log PMF of a Binomoal distribution, up to a constant. Assumes n >= x, p != 0 and p != 1

    the expression with constants would be:
               x*log(p) + (n-x)*log(1-p) + log(n!) - log(x!) - log((n-x)!)
    """
    if n==0:
        return 0
    elif x==0:
        return n*log(1-p)
    elif n==x:
        return x*log(p)
    else:
        return x*log(p) + (n-x)*log(1-p)


@numba.vectorize([numba.float64(numba.int32, numba.float64, numba.float64)])
def dl_negabinom_up_to_c(x, mean, over):
    """
    overdispersed poisson aka negative binomial aka Polya. Assumes mean > 0, ignores the constant

    nbinom.pmf(x) = choose(x+f-1, f-1) * p**x * (1-p)**f

    the expression with constants would be:
                f = 1 / over
                p = mean / (mean+f)
                x*log(p) + f*log(1-p) + lgamma(x+f) - lgamma(f) - lgamma(x+1)
                or
                x*log(mean) - (x+f)*log(mean+f) + f*log(f) + lgamma(x+f) - lgamma(f) - lgamma(x+1)
    """
    f = 1 / over
    #p = mean / (mean+f)
    if x == 0:
        return -f*log(mean+f)
    else:
        return x*log(mean) - (x+f)*log(mean+f)


if __name__ == '__main__':
    epidemic = Epidemic()
    print(epidemic)
    print(epidemic.Likelihood)
    epidemic.for_record()
    print(epidemic.I.sum(0))
    print(epidemic.all_priors())
    print(epidemic.all_likelihood_factors())

    #D = M[50]._synthetic_data()
    #for t, name in enumerate('CONF WARD ICUC DEAD'.split()):
        #print()
        #print()
        #print(name+' = [')
        #name = ['detected cases', 'hospitalizations', 'IC hospitalizations', 'fatal cases'][t]
        #print(f'    # {name} among non-vaccinated')
        #print(str(D[t][0]).replace('], [', '],\n     ['))
        #print(f'    # {name} among these with the first dose')
        #print(str(D[t][1]).replace('], [', '],\n     ['))
        #print(f'    # {name} among these within at least 1 week after the second dose')
        #print(str(D[t][2]).replace('], [', '],\n     ['))
        #print(f'    # {name} among these within at least 1 week after the third dose')
        #print(str(D[t][3]).replace('], [', '],\n     ['))
        #print('    ]')

