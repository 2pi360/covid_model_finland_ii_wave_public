if False:  # standard profiling


    import MCMC
    import cProfile as prf
    import pstats


    #MCMC.MAIN(warmup=0, iterations_n=1000, trial_name="C:/history/profiling")

    prf.run('MCMC.MAIN(warmup=0, iterations_n=2000, trial_name="C:/history/profiling")', 'profiling_output')

    p = pstats.Stats('profiling_output')

    p.sort_stats('cumulative').print_stats(50)
    p.sort_stats('tottime').print_stats(50)

    p.sort_stats('cumulative').print_stats('parameters.py:')
    p.sort_stats('tottime').print_stats('useful.py:')
    p.sort_stats('tottime').print_stats('model.py:')
    p.sort_stats('tottime').print_stats('prob_prog.py:')
    p.sort_stats('tottime').print_stats('numpy')


else:  # self-made profiling for model components

    from datetime import datetime
    import prob_prog
    prob_prog._PROFILING = True

    import MCMC
    total_start = datetime.now()
    MCMC.MAIN(warmup=0, iterations_n=1000, trial_name="C:/history/profiling")

    total = (datetime.now() - total_start)
    print(total)
    print()

    for name, prf in sorted(prob_prog._PROFILING_RESULTS.items(), key=lambda item: item[1]['recompute_time'], reverse=True):

        prf['ratio'] = prf['recompute_time']/prf['recompute_calls'] * 1000 if prf['recompute_calls'] > 0 else '-'
        prf['percent'] = '{:.1f}'.format(prf['recompute_time']/total.total_seconds() * 100) if prf['recompute_calls'] > 0 else '-'

        print('{name:>30}: {percent:>4}  {recompute_time:.3f} {recompute_calls:>7}   {total_time:.3f} {total_calls:>7}     {arguments_time:.3f}    {ratio}'.format(name=name, **prf))


